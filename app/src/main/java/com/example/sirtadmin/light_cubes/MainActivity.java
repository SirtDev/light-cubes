package com.example.sirtadmin.light_cubes;

import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    public Button[] btn = new Button[16];
    private int[] btn_id = {R.id.button1, R.id.button2, R.id.button3, R.id.button4, R.id.button5, R.id.button6, R.id.button7, R.id.button8, R.id.button9,
            R.id.button10, R.id.button11, R.id.button12, R.id.button13, R.id.button14, R.id.button15, R.id.button16};


    Handler handler1 = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // associate button array elements with the buttons defined in layout.xml
        for (int a = 0; a < btn.length; a++) {
            btn[a] = (Button) findViewById(btn_id[a]);
        }

        int btn_id = R.id.button1;
        System.out.println(btn_id);
    }

    @Override
    protected void onStart() {
        super.onStart();



        cycleColors();
    }


    public void cycleColors() {
        // separate thread from UI, that delays for 500ms

        for (int i = 0; i<btn.length ;i++) {
            final int p = i;
            handler1.postDelayed(new Runnable() {

                @Override
                public void run() {
                    //Button btn5 = btn[random.nextInt(btn.length)];
                    btn[p].setBackgroundColor(Color.BLUE);
                }
            }, 500 * i);
        }
    }






}
